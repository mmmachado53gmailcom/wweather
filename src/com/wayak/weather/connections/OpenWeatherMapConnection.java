package com.wayak.weather.connections;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;

import com.wayak.weather.R;
import com.wayak.weather.models.City;
import com.wayak.weather.models.DayWeatherData;
import com.wayak.weather.models.HourWeatherData;
import com.wayak.weather.models.WeatherData;
import com.wayak.weather.db.Schema;

public final class OpenWeatherMapConnection extends WeatherConnection {
	private static final String URL_BASE="http://api.openweathermap.org/data/2.5/";
	private static OpenWeatherMapConnection instance=null;
	private static Context ctx=null;
	
	private OpenWeatherMapConnection(){}
	public static WeatherConnection get(Context contexto) {
		// TODO Auto-generated method stub
		ctx=contexto;
		if (instance==null) {
			instance=new OpenWeatherMapConnection();
		}
		return instance;
	}
	@Override
	public String getSearchCityURL(Double lat, Double lon) {
		//http://api.openweathermap.org/data/2.5/find?lat=57&lon=-2.15
		return URL_BASE+"find?lat="+lat+"&lon="+lon;
	}

	@Override
	public String getSearchCityURL(String CityName) {
		if(CityName.length()<3){
			return null;
		}
		//http://api.openweathermap.org/data/2.5/find?q=London&mode=json
		return URL_BASE+"find?q="+Uri.encode(CityName)+"&mode=json";
	}

	
	
	public List<City> searchCity(HttpGet get) throws ClientProtocolException, JSONException, IOException{
		List<City> result=new ArrayList<City>();
		JSONObject json=Helper.getJsonObject(ctx, get);
		int code=json.getInt("cod");
		if (code==200) {
			JSONArray list=json.getJSONArray("list");
			int total=list.length();
			for (int i = 0; i < total; i++) {
				City city=new City();
				JSONObject obj=list.getJSONObject(i);
				city.name=obj.getString("name");
				city.id=obj.getInt("id");
				JSONObject coord=obj.getJSONObject("coord");
				city.lat=coord.getDouble("lat");
				city.lon=coord.getDouble("lon");
				JSONObject sys=obj.getJSONObject("sys");
				city.country=sys.getString("country");
				result.add(city);
				
			}
		}
		return result;
	}
	
	
	
	@Override
	public String getForecastHourURL(int cityId){
		// http://api.openweathermap.org/data/2.5/forecast?id=524901
		return URL_BASE+"forecast?id="+cityId;
	}

	@Override
	public String getForecastHourURL(String cityName){
		// http://api.openweathermap.org/data/2.5/forecast?q=London
		return URL_BASE+"forecast?q="+Uri.encode(cityName);
	}
	@Override
	public String getForecastHourURL(Double lat, Double lon)  {
		// http://api.openweathermap.org/data/2.5/forecast?lat=35&lon=139
		return URL_BASE+"forecast?lat="+lat+"&lon="+lon;
		
	}

	
	public List<HourWeatherData> getForecastHour(HttpGet get)  throws ClientProtocolException, JSONException, IOException{
		JSONObject json=Helper.getJsonObject(ctx, get);
		JSONArray list=json.getJSONArray("list");
		int total=list.length();
			if (total>12) {
			total=12;
		}
		Vector<HourWeatherData> result=new Vector<HourWeatherData>();
		for (int i = 0; i < total; i++) {
			JSONObject item=list.getJSONObject(i);
			HourWeatherData data=new HourWeatherData();
			data.type=Schema.WeatherDatas.TYPE_HOUR;
			data.dt=item.getInt("dt");
			JSONObject main=item.getJSONObject("main");
			data.temp=main.getDouble("temp");
			data.humidity=main.getInt("humidity");				
			if (item.has("weather")) {
				JSONArray weather=item.getJSONArray("weather");
				if (weather.length()>0) {
					JSONObject w=weather.getJSONObject(0);
					data.description_code=getDescription(w.getInt("id"));						
					data.icon=getIcon(w.getString("icon"));	
				}
			}
				
			result.add(data);
			}
		return result;
		
	}
	
	
	@Override
	public String getForecastDayURL(int cityId) {
		//http://api.openweathermap.org/data/2.5/forecast/daily?id=524901&cnt=10
		return URL_BASE+"forecast/daily?id="+cityId+"&cnt=16";
	}

	@Override
	public String getForecastDayURL(String cityName) {
		// http://api.openweathermap.org/data/2.5/forecast/daily?q=London&cnt=10
		return URL_BASE+"forecast/daily?q="+Uri.encode(cityName)+"&cnt=16";
		
	}
	@Override
	public String getForecastDayURL(Double lat, Double lon) {
		// http://api.openweathermap.org/data/2.5/forecast/daily?lat=35&lon=139&cnt=10
		return URL_BASE+"forecast/daily?lat="+lat+"&lon="+lon+"&cnt=16";
	}
	
	public List<DayWeatherData> getForecastDay(HttpGet get) throws ClientProtocolException, JSONException, IOException{
		JSONObject json=Helper.getJsonObject(ctx, get);
		
				JSONArray list=json.getJSONArray("list");
				int total=list.length();
				List<DayWeatherData> result=new ArrayList<DayWeatherData>();
				for (int i = 0; i < total; i++) {
					JSONObject item=list.getJSONObject(i);
					DayWeatherData data=new DayWeatherData();
					data.type=Schema.WeatherDatas.TYPE_DAY;
					data.dt=item.getInt("dt");	
					JSONObject temp=item.getJSONObject("temp");
					data.temp_max=temp.getDouble("max");
					data.temp_min=temp.getDouble("min");
					data.pressure=item.getDouble("pressure");
					data.humidity=item.getInt("humidity");
					if (item.has("weather")) {
						JSONArray weather=item.getJSONArray("weather");
						if (weather.length()>0) {
							JSONObject w=weather.getJSONObject(0);
							data.icon=getIcon(w.getString("icon"));
							data.description_code=getDescription(w.getInt("id"));
						}
						if (item.has("speed")) {
							data.wind_speed=item.getDouble("speed");
						}
						if (item.has("deg")) {
							data.wind_deg=item.getDouble("deg");
						}
						
						
						
					}
					result.add(data);
				}
				return result;
			
	}
	
	
	
	
	@Override
	public String getOverViewURL(String cityName) {
		// http://api.openweathermap.org/data/2.5/weather?q=London
		return URL_BASE+"weather?q="+Uri.encode(cityName);
		
	}
	@Override
	public String getOverViewURL(int cityID) {
		//http://api.openweathermap.org/data/2.5/weather?id=2172797
		return URL_BASE+"weather?id="+cityID;
	}
	@Override
	public String getOverViewURL(Double lat, Double lon) {
		//http://api.openweathermap.org/data/2.5/weather?lat=35&lon=139
		return URL_BASE+"weather?lat="+lat+"&lon="+lon;
	}

	public DayWeatherData getOverView(HttpGet get) throws JSONException, ClientProtocolException, IOException{
		JSONObject json=Helper.getJsonObject(ctx, get);
			DayWeatherData data=new DayWeatherData();
			data.type=Schema.WeatherDatas.TYPE_OVERVIEW;
			data.dt=json.getInt("dt");
			JSONObject sys=json.getJSONObject("sys");
			data.sunrise=sys.getInt("sunrise");
			data.sunset=sys.getInt("sunset");
			if (json.has("weather")) {
				JSONArray weather=json.getJSONArray("weather");
				if (weather.length()>0) {
					JSONObject w=weather.getJSONObject(0);
					data.icon=getIcon(w.getString("icon"));
					data.description_code=getDescription(w.getInt("id"));
				}
			}
			JSONObject main=json.getJSONObject("main");
			data.temp=main.getDouble("temp");
			data.pressure=main.getDouble("pressure");
			data.humidity=main.getDouble("humidity");
			if (main.has("temp_min") && main.has("temp_max")) {
				data.temp_max=main.getDouble("temp_max");
				data.temp_min=main.getDouble("temp_min");
			}
			if (json.has("wind")) {
				JSONObject wind=json.getJSONObject("wind");
				data.wind_deg=wind.getDouble("deg");
				data.wind_speed=wind.getDouble("speed");
			}			
		return data;
	}
	
	
	private int getIcon(String iconString){
		if (iconString.equals("01d")) {
			return WeatherData.ICON_SKY_CLEAR_DAY;
		}else if(iconString.equals("01n")){
			return WeatherData.ICON_SKY_CLEAR_NIGHT;
		}else if(iconString.equals("02d")){
			return WeatherData.ICON_FEW_CLOUDS_DAY;
		}else if (iconString.equals("02n")) {
			return WeatherData.ICON_FEW_CLOUDS_NIGHT;
		}else if(iconString.equals("03d")){
			return WeatherData.ICON_SCATTERD_CLOUDS;
		}else if (iconString.equals("03n")) {
			return WeatherData.ICON_SCATTERD_CLOUDS;
		}else if(iconString.equals("04d")){
			return WeatherData.ICON_BROKEN_CLOUDS_DAY;
		}else if (iconString.equals("04n")) {
			return WeatherData.ICON_BROKEN_CLOUDS_NIGHT;
		}else if(iconString.equals("09d")){
			return WeatherData.ICON_SHOWER_RAIN_DAY;
		}else if (iconString.equals("09n")) {
			return WeatherData.ICON_SHOWER_RAIN_NIGHT;
		}else if(iconString.equals("10d")){
			return WeatherData.ICON_RAIN;
		}else if (iconString.equals("10n")) {
			return WeatherData.ICON_RAIN;
		}else if(iconString.equals("11d")){
			return WeatherData.ICON_THUNDERSTORM;
		}else if (iconString.equals("11n")) {
			return WeatherData.ICON_THUNDERSTORM;
		}else if(iconString.equals("13d")){
			return WeatherData.ICON_SNOW;
		}else if (iconString.equals("13n")) {
			return WeatherData.ICON_SNOW;
		}else if(iconString.equals("50d")){
			return WeatherData.ICON_MISTY;
		}else if (iconString.equals("50n")) {
			return WeatherData.ICON_MISTY;
		}
		return WeatherData.ICON_NOT_AVAILABLE;
	}
	
	private int getDescription(int id){
		switch (id) {
		case 200:
			return R.string.thunderstorm_with_light_rain;
		case 201:
			return R.string.thunderstorm_with_rain;
		case 202:
			return R.string.thunderstorm_with_heavy_rain;
		default:
			return R.string.unknown;
		}
		/*if (id==200) {
			return R.string.thunderstorm_with_light_rain;
		}else if(){}
		return R.string.unknown;*/
	}
	
	
	


}
