package com.wayak.weather;

import java.io.IOException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONException;
import org.json.JSONObject;

import com.wayak.weather.connections.Helper;
import com.wayak.weather.connections.OpenWeatherMapConnection;
import com.wayak.weather.db.CitiesDataSource;
import com.wayak.weather.helpers.BroadcastHelper;
import com.wayak.weather.models.City;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class SearchActivity extends Activity implements TextWatcher, OnItemClickListener{
	private EditText searchTXT;
	private ListView resultList;
	public ProgressBar loading;
	public OpenWeatherMapConnection connection;
	public ArrayAdapter<City> resultAdapter;
	private SearchTask task=null;
	private CitiesDataSource cityDataSource;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setResult(RESULT_CANCELED);
		cityDataSource=CitiesDataSource.get(getApplicationContext());
		connection=(OpenWeatherMapConnection) OpenWeatherMapConnection.get(getApplicationContext());
		setContentView(R.layout.activity_search);
		searchTXT=(EditText) findViewById(R.id.search_txt);
		resultList= (ListView) findViewById(R.id.listView1);
		loading=(ProgressBar) findViewById(R.id.progressBar1);
		loading.setVisibility(View.GONE);
		searchTXT.addTextChangedListener(this);
		resultAdapter=getAdapter();
		resultList.setAdapter(resultAdapter);
		resultList.setOnItemClickListener(this);
	}
	
	/*
	 * Make adapter for ListView
	 * */
	private ArrayAdapter<City> getAdapter(){
		return new ArrayAdapter<City>(getApplicationContext(), 0){
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				ListHolder holder;
				if (convertView==null) {
					convertView=getLayoutInflater().inflate(android.R.layout.simple_list_item_1, null);
					holder=new ListHolder();
					holder.txt1=(TextView) convertView.findViewById(android.R.id.text1);
					convertView.setTag(holder);
				}
				holder=(ListHolder) convertView.getTag();
				City city=getItem(position);
				holder.txt1.setText(city.name+", "+city.country);
				return convertView;
			}
		};
	}

	/*
	 * TextWatcher functions for searchTXT
	 * */
	@Override
	public void afterTextChanged(Editable text) {}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,int arg3) {	}
	
	@Override
	public void onTextChanged(CharSequence text, int arg1, int arg2, int arg3) {
		Log.i("onTextChanged","onTextChanged");
		if (text.length()<3) {return;}
		if (task!=null) {
			task.stop();
		}
		task=new SearchTask(this);
		String name=Normalizer.normalize(text, Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");;
	
		task.execute(name);
	}
	/*
	 * End of TextWatcher functions
	 * */
	
	
	/*
	 * Click listener, fires when select a city
	 * */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		ArrayAdapter<City> adapter=(ArrayAdapter<City>) parent.getAdapter();
		City city=adapter.getItem(position);
		new SearchTimeZoneTask(this, city).execute(city.lat, city.lon);
		/*cityDataSource.open();
		cityDataSource.saveCity(city);
		cityDataSource.close();
		BroadcastHelper.sendBroadcast(WeatherService.ACTION_UPDATE_REQUEST, getApplicationContext());
		Intent intent=getIntent();
		intent.putExtra("cityID", city.id);
		setResult(RESULT_OK,intent);
		finish();*/
		Log.i("click", city.name+", "+city.country);
	}
	
	/*
	 * AsyncTask (Tread)  
	 * */
	
	private class SearchTask extends AsyncTask<String, Integer, List<City>>{
		private SearchActivity parent;
		private static final int ERROR_CONNECTION=1;
		private static final int ERROR_CONNECTIONB=2;
		private static final int ERROR_PARSE=3;
		private int error=0;
		private HttpGet get=null;
		public SearchTask(SearchActivity pa){
			parent=pa;
		}

		@Override
		protected List<City> doInBackground(String... params) {
			String cityName=params[0];
			String url=parent.connection.getSearchCityURL(cityName);
			Log.i("URL",cityName+" -- "+url);
			get=new HttpGet(url);
			try {
				return parent.connection.searchCity(get);
			} catch (ClientProtocolException e) {
				e.printStackTrace();
				error=ERROR_CONNECTION;
				cancel(true);
			} catch (JSONException e) {
				e.printStackTrace();
				error=ERROR_PARSE;
				cancel(true);
			} catch (IOException e) {
				e.printStackTrace();
				error=ERROR_CONNECTIONB;
				cancel(true);
			}
			return new ArrayList<City>();
		}
		@Override
		protected void onCancelled() {
			super.onCancelled();
			switch (error) {
			case ERROR_CONNECTION:
				parent.onConnectionError(1);
				break;
			case ERROR_CONNECTIONB:
				parent.onConnectionError(2);
				break;
			case ERROR_PARSE:
				parent.onParseError();
				break;
			default:
				break;
			}
			parent.loading.setVisibility(View.GONE);
		}
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			parent.loading.setVisibility(View.VISIBLE);
		}
		@Override
		protected void onPostExecute(List<City> result) {
			super.onPostExecute(result);
			parent.onRequestComplete(result);
			parent.loading.setVisibility(View.GONE);
		}
		public void stop(){
			if(getStatus().equals(AsyncTask.Status.RUNNING)){
				cancel(true);
				if (get!=null) {
					get.abort();
				}
				
			}
			
		}
	}
	
	/*
	 * end of AsyncTask (Tread)  
	 * */
	
	
	private class SearchTimeZoneTask extends AsyncTask<Double, String, String>{
		private SearchActivity pa;
		private City city;
		private String error="";
		@Override
		protected void onProgressUpdate(String... values) {
			// TODO Auto-generated method stub
			Toast.makeText(pa, values[0], Toast.LENGTH_LONG).show();
			super.onProgressUpdate(values);
		}
		public SearchTimeZoneTask(SearchActivity parent, City city){
			pa=parent;
			this.city=city;
		}
		@Override
		protected void onPreExecute() {
			pa.disableView();
			super.onPreExecute();
		}
		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			//Toast.makeText(pa, error, Toast.LENGTH_LONG).show();
			pa.enabledView();
			super.onCancelled();
		}
		@Override
		protected String doInBackground(Double... params) {
			
			Double lat=params[0];
			Double lon=params[1];
			String url="https://maps.googleapis.com/maps/api/timezone/json?location="+lat+","+lon+"&timestamp="+(new Date().getTime()/1000L)+"&sensor=false";
			Log.i("GOOGLEURL",url);
			HttpGet get=new HttpGet(url);
			publishProgress(url);
			try {
				JSONObject json=Helper.getJsonObject(pa.getApplicationContext(), get);
				String status=json.getString("status");
				if (!status.contains("OK")) {
					cancel(true);
				}
				return json.getString("timeZoneId");
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				error+="ClientProtocolException";
				e.printStackTrace();
				cancel(true);
			} catch (JSONException e) {
				error+="JSONException";
				// TODO Auto-generated catch block
				e.printStackTrace();
				cancel(true);
			} catch (IOException e) {
				error+="IOException";
				// TODO Auto-generated catch block
				e.printStackTrace();
				cancel(true);
			}
			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			city.timeZoneID=result;
			pa.saveCity(city);
		}
		
	}
	
	
	public void onConnectionError(int type){
		resultAdapter.clear();
		resultAdapter.notifyDataSetChanged();
		Log.i("connection_error", type+"");
	}
	
	public void onParseError(){
		resultAdapter.clear();
		resultAdapter.notifyDataSetChanged();
		Log.i("onParseError", "onParseError");
	}
	
	public void onRequestComplete(List<City> cities){
		Log.i("onRequestComplete", "cities: "+cities.size());
		resultAdapter.clear();
		for (City city:cities) {
			if (city.name.length()>0) {
				resultAdapter.add(city);
			}
			
		}
		resultAdapter.notifyDataSetChanged();
	}
	
	private class ListHolder{
		TextView txt1;
	}
	
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if (task!=null) {
			task.stop();
		}
	}	
	
	protected void disableView(){
		this.searchTXT.setText("");
		this.searchTXT.setEnabled(false);
		this.resultAdapter.clear();
		this.resultAdapter.notifyDataSetChanged();
	}
	
	protected void enabledView(){
		this.searchTXT.setEnabled(true);
	}
	
	
	protected void saveCity(City city){
		cityDataSource.open();
		cityDataSource.saveCity(city);
		cityDataSource.close();
		BroadcastHelper.sendBroadcast(WeatherService.ACTION_UPDATE_REQUEST, getApplicationContext());
		Intent intent=getIntent();
		intent.putExtra("cityID", city.id);
		setResult(RESULT_OK,intent);
		finish();
	}
	
}
