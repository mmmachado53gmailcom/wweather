package com.wayak.weather.models;

import com.wayak.weather.helpers.ConversorHelper;
import com.wayak.weather.helpers.CursorHelper;
import com.wayak.weather.db.Schema;

import android.content.ContentValues;
import android.database.Cursor;

public final class DayWeatherData extends WeatherData{
	
	public double pressure=0.0;
	public double wind_speed=0.0;
	public double wind_deg=0.0;
	public double humidity=0.0;
	public double temp_min=0.0;
	public double temp_max=0.0;
	public int temp_min_celsius=0;
	public int temp_min_fahrenheit=0;
	public int temp_max_fahrenheit=0;
	public int temp_max_celsius=0;
	public int sunset=0;
	public int sunrise=0;
	
	
	@Override
	public ContentValues toContentValues() {
		ContentValues result=super.toContentValues();
		result.put(Schema.WeatherDatas.KEY_TYPE, type);

		result.put(Schema.WeatherDatas.KEY_PRESSURE, pressure);
		result.put(Schema.WeatherDatas.KEY_WIND_DEG, wind_deg);
		result.put(Schema.WeatherDatas.KEY_WIND_SPEED, wind_speed);
		result.put(Schema.WeatherDatas.KEY_HUMIDITY, humidity);
		result.put(Schema.WeatherDatas.KEY_TEMP_MAX, temp_max);
		result.put(Schema.WeatherDatas.KEY_TEMP_MIN, temp_min);	
		result.put(Schema.WeatherDatas.KEY_SUNRISE, sunrise);
		result.put(Schema.WeatherDatas.KEY_SUNSET, sunset);
		return result;
	}
	@Override
	public void setFromCursor(Cursor cursor) {
		super.setFromCursor(cursor);
		
		this.pressure=CursorHelper.getDouble(cursor, Schema.WeatherDatas.KEY_PRESSURE);
		this.temp_max=CursorHelper.getDouble(cursor, Schema.WeatherDatas.KEY_TEMP_MAX);
		this.temp_min=CursorHelper.getDouble(cursor, Schema.WeatherDatas.KEY_TEMP_MIN);
		this.type=CursorHelper.getInt(cursor, Schema.WeatherDatas.KEY_TYPE);
		this.wind_deg=CursorHelper.getDouble(cursor, Schema.WeatherDatas.KEY_WIND_DEG);
		this.wind_speed=CursorHelper.getDouble(cursor, Schema.WeatherDatas.KEY_WIND_SPEED);
		this.humidity=CursorHelper.getDouble(cursor, Schema.WeatherDatas.KEY_HUMIDITY);
		this.sunrise=CursorHelper.getInt(cursor, Schema.WeatherDatas.KEY_SUNRISE);
		this.sunset=CursorHelper.getInt(cursor, Schema.WeatherDatas.KEY_SUNSET);
		this.temp_max_celsius=ConversorHelper.kelvinToCelsius((int)this.temp_max);
		this.temp_max_fahrenheit=ConversorHelper.kelvinToFahrenheit((int)this.temp_max);
		this.temp_min_celsius=ConversorHelper.kelvinToCelsius((int)this.temp_min);
		this.temp_min_fahrenheit=ConversorHelper.kelvinToFahrenheit((int)this.temp_min);
	}
}
