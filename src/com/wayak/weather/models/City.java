package com.wayak.weather.models;


import java.io.Serializable;
import java.util.List;
import android.content.ContentValues;
import android.database.Cursor;
import com.wayak.weather.helpers.CursorHelper;
import com.wayak.weather.db.Schema;

public final class City implements Model, Serializable{
	
	public static final String NO_TIMEZONE="no_timezone";
	
	public DayWeatherData overView;
	public List<DayWeatherData> daysWeathers=null;
	public List<HourWeatherData> hoursWeathers=null;
	
	public String name="";
	public Double lat=0.0;
	public Double lon=0.0;
	public String country="";
	public int id=0;
	public Long last_update=0L;

	
	public String timeZoneID=NO_TIMEZONE;
	
	
	
	@Override
	public ContentValues toContentValues() {
		ContentValues result=new ContentValues();
		result.put(Schema.Cityes.KEY_COUNTRY, country);
		result.put(Schema.Cityes.KEY_ID, id);
		result.put(Schema.Cityes.KEY_LAT, lat);
		result.put(Schema.Cityes.KEY_LON, lon);
		result.put(Schema.Cityes.KEY_NAME, name);
		result.put(Schema.Cityes.KEY_LAST_UPDATE, last_update);
		result.put(Schema.Cityes.KEY_TIMEZONE_ID, timeZoneID);
		return result;
	}
	@Override
	public void setFromCursor(Cursor cursor) {
		// TODO Auto-generated method stub
		this.country=CursorHelper.getString(cursor, Schema.Cityes.KEY_COUNTRY);
		this.name=CursorHelper.getString(cursor, Schema.Cityes.KEY_NAME);
		this.id=CursorHelper.getInt(cursor, Schema.Cityes.KEY_ID);
		this.lat=CursorHelper.getDouble(cursor, Schema.Cityes.KEY_LAT);
		this.lon=CursorHelper.getDouble(cursor, Schema.Cityes.KEY_LON);
		this.last_update=CursorHelper.getLong(cursor, Schema.Cityes.KEY_LAST_UPDATE);
		this.timeZoneID=CursorHelper.getString(cursor, Schema.Cityes.KEY_TIMEZONE_ID);
	} 
	
}
