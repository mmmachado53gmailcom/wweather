package com.wayak.weather.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.wayak.weather.config.Config;
import com.wayak.weather.libs.ContentHelper;
import com.wayak.weather.models.City;
import com.wayak.weather.models.DayWeatherData;
import com.wayak.weather.models.HourWeatherData;
import com.wayak.weather.models.WeatherData;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public final class CitiesDataSource implements DataSource{
	
	private Context ctx;
	private SQLiteDatabase db;
	private AppSQLiteHelper appSQLiteHelper;
	private static CitiesDataSource instance=null;
	private String TABLE_NAME=Schema.Cityes.TABLE_NAME;
	private Config conf;
	
	private CitiesDataSource(Context context){
		ctx=context;
		conf=Config.get(context);
		appSQLiteHelper=new AppSQLiteHelper(ctx);
	}
	public static CitiesDataSource get(Context context){
		instance= instance == null ? new CitiesDataSource(context) : instance;
		return instance;
	}
	@Override
	public void open() {
		db=appSQLiteHelper.getWritableDatabase();
	}
	@Override
	public void close() {
		appSQLiteHelper.close();
	}
	
	public void saveCity(City model){
		db.replace(TABLE_NAME,null,model.toContentValues());
	
	}
	
	public List<City> getAllCities(Boolean includeData){
		List<City> result=new ArrayList<City>();
		Cursor cursor=db.query(TABLE_NAME, null, null, null, null, null, null);
		if (cursor.getCount()==0) {
			return result;
		}
		if (cursor.moveToFirst()) {
			do{
				City city=new City();
				city.setFromCursor(cursor);
				if (includeData) {
					city.overView=ContentHelper.getOverView(city.id, ctx);
					city.daysWeathers=ContentHelper.getDaysData(city.id, ctx);
					city.hoursWeathers=ContentHelper.getHoursData(city.id, ctx);
				}
				result.add(city);
			}while(cursor.moveToNext());
		}
		cursor.close();
		return result;
	}
	
	public City getCity(int id){
		City result=new City();
		Cursor cursor=db.query(TABLE_NAME, null, Schema.Cityes.KEY_ID+"="+id, null, null, null, null);
		if (cursor.moveToFirst()) {
			result.setFromCursor(cursor);
		}
		cursor.close();
		return result;
	}
	
	public List<City> getOutdatedCities(){
		List<City> result=new ArrayList<City>();
		Long now=new Date().getTime()-(60000*(conf.getUpdateTime()-1));
		Cursor cursor=db.query(TABLE_NAME, null,Schema.Cityes.KEY_LAST_UPDATE+" < "+now, null,null,null,null);
		if (cursor.moveToFirst()) {
			do {
				City city=new City();
				city.setFromCursor(cursor);
				result.add(city);
			} while (cursor.moveToNext());
		}
		cursor.close();
		return result;
	}
	public void saveForecast(List<DayWeatherData> dayForecast,List<HourWeatherData> hourForecast, DayWeatherData overView, int cityId){
		Log.i("CitiesDataSource", "saveForecast");
		Log.i("CitiesDataSource", "dayForecast: "+dayForecast.size());
		Log.i("CitiesDataSource", "hourForecast: "+hourForecast.size());
		
		db.delete(Schema.WeatherDatas.TABLE_NAME, Schema.WeatherDatas.KEY_CITY_ID+" = "+cityId, null);
		overView.city_id=cityId;
		db.insert(Schema.WeatherDatas.TABLE_NAME,null,overView.toContentValues());
		for(DayWeatherData data:dayForecast){
			data.city_id=cityId;
			db.insert(Schema.WeatherDatas.TABLE_NAME,null,data.toContentValues());
		}
		for(HourWeatherData data:hourForecast){
			data.city_id=cityId;
			db.insert(Schema.WeatherDatas.TABLE_NAME,null,data.toContentValues());
		}
	}
	
}
