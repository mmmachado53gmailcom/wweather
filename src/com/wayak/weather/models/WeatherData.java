package com.wayak.weather.models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import android.content.ContentValues;
import android.database.Cursor;

import com.wayak.weather.helpers.ConversorHelper;
import com.wayak.weather.helpers.CursorHelper;
import com.wayak.weather.db.Schema;
import com.wayak.weather.db.Schema.WeatherDatas;

public  class WeatherData implements Model{
	//icons
	
		public static final int ICON_NOT_AVAILABLE=0;
		public static final int ICON_SKY_CLEAR_DAY=1;
		public static final int ICON_SKY_CLEAR_NIGHT=2;
		public static final int ICON_FEW_CLOUDS_DAY=3;
		public static final int ICON_FEW_CLOUDS_NIGHT=4;
		public static final int ICON_SCATTERD_CLOUDS=5;
		
		public static final int ICON_BROKEN_CLOUDS_DAY=6;
		public static final int ICON_BROKEN_CLOUDS_NIGHT=7;
		public static final int ICON_SHOWER_RAIN_DAY=8;
		public static final int ICON_SHOWER_RAIN_NIGHT=9;
		public static final int ICON_RAIN=10;
		
		public static final int ICON_THUNDERSTORM=11;
		
		public static final int ICON_SNOW=12;
		
		public static final int ICON_MISTY=13;
		
		
		
	public int city_id=0;
		
	//Time of data receiving in unixtime GMT
	public int dt=0;
	public int type;
	public int icon=ICON_NOT_AVAILABLE;
	public int humidity=0;
	public int description_code;
	public double temp=0.0;
	public int tempCelsius=0;
	public int tempFahrenheit=0;
	
	public Date date;
	
	public String dateST;
	public String dateSTB;
	
	
	@Override
	public ContentValues toContentValues(){
		ContentValues result=new ContentValues();
		result.put(Schema.WeatherDatas.KEY_CITY_ID, city_id);
		result.put(Schema.WeatherDatas.KEY_DESCRIPTION_CODE, description_code);
		result.put(Schema.WeatherDatas.KEY_DT, dt);
		result.put(Schema.WeatherDatas.KEY_HUMIDITY, humidity);
		result.put(Schema.WeatherDatas.KEY_ICON, icon);
		
		result.put(Schema.WeatherDatas.KEY_TEMP, temp);
			
		return result;
	}
	@Override
	public void setFromCursor(Cursor cursor){
		this.city_id=CursorHelper.getInt(cursor, Schema.WeatherDatas.KEY_CITY_ID);
		this.description_code=CursorHelper.getInt(cursor, Schema.WeatherDatas.KEY_DESCRIPTION_CODE);
		this.dt=CursorHelper.getInt(cursor, Schema.WeatherDatas.KEY_DT);
		this.humidity=CursorHelper.getInt(cursor, Schema.WeatherDatas.KEY_HUMIDITY);
		this.icon=CursorHelper.getInt(cursor, Schema.WeatherDatas.KEY_ICON);
		this.temp=CursorHelper.getDouble(cursor, Schema.WeatherDatas.KEY_TEMP);
		this.tempCelsius=ConversorHelper.kelvinToCelsius((int)this.temp);
		this.tempFahrenheit=ConversorHelper.kelvinToFahrenheit((int)this.temp);
		this.date=new Date(this.dt*1000L);
		SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd HH:mm z");
		df.setTimeZone(TimeZone.getTimeZone("GMT"));
		this.dateST=df.format(date);
		this.dateSTB=date.toString();
		/*SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd HH:mm z");
		df.setTimeZone(TimeZone.getTimeZone("GMT"));
		this.dateST=df.format(this.date);
		SimpleDateFormat dfb=new SimpleDateFormat("yyyy-MM-dd HH:mm z");
		this.dateSTB=dfb.format(date);
		
		SimpleDateFormat dfc=new SimpleDateFormat("yyyy-mm-dd HH:mm");
		String GMTdate=dfc.format(date)+" GMT";
		SimpleDateFormat outPutDF=new SimpleDateFormat("yyyy-mm-dd HH:mm z");
		Date d;
		try {
			d = outPutDF.parse(GMTdate);
			this.dateST=dfb.format(d);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			this.dateST="mames";
			e.printStackTrace();
		}*/
		
		
	}
}
