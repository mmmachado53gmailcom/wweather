package com.wayak.weather.libs;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.wayak.weather.WeatherContentProvider;
import com.wayak.weather.models.City;
import com.wayak.weather.models.DayWeatherData;
import com.wayak.weather.models.HourWeatherData;

public class ContentHelper {
	public static DayWeatherData getOverView(int cityId, Context ctx){
		DayWeatherData result=null;
		Uri uri=Uri.parse("content://" + WeatherContentProvider.AUTHORITY +"/"+WeatherContentProvider.PATH_OVER_VIEW_FORECAST+"/"+cityId);
		Cursor cursor=ctx.getContentResolver().query(uri, null, null, null, null);
		if (cursor.getCount()>0) {
			cursor.moveToFirst();
			result=new DayWeatherData();
			result.setFromCursor(cursor);
		}
		cursor.close();
		return result;
		
	}
	
	public static List<DayWeatherData> getDaysData(int cityId, Context ctx){
		List<DayWeatherData> result=new ArrayList<DayWeatherData>();
		Uri uri=Uri.parse("content://" + WeatherContentProvider.AUTHORITY +"/"+WeatherContentProvider.PATH_DAY_FORECAST+"/"+cityId);
		Cursor cursor=ctx.getContentResolver().query(uri, null, null, null, null);
		
		if (cursor.getCount()>0) {
			cursor.moveToFirst();
			do {
				DayWeatherData day=new DayWeatherData();
				day.setFromCursor(cursor);
				result.add(day);
			} while (cursor.moveToNext());
		}
		cursor.close();
		return result;
		
	}
	public static List<HourWeatherData> getHoursData(int cityId, Context ctx){
		List<HourWeatherData> result=new ArrayList<HourWeatherData>();
		Uri uri=Uri.parse("content://" + WeatherContentProvider.AUTHORITY +"/"+WeatherContentProvider.PATH_HOUR_FORECAST+"/"+cityId);
		Cursor cursor=ctx.getContentResolver().query(uri, null, null, null, null);
		
		if (cursor.getCount()>0) {
			cursor.moveToFirst();
			do {
				HourWeatherData hour=new HourWeatherData();
				hour.setFromCursor(cursor);
				result.add(hour);
			} while (cursor.moveToNext());
		}
		cursor.close();
		return result;
	}
	
	public static City getCity(int cityId, Context ctx){
		City result=null;
		Uri uri=Uri.parse("content://" + WeatherContentProvider.AUTHORITY +"/"+WeatherContentProvider.PATH_CITY+"/"+cityId);
		Cursor cursor=ctx.getContentResolver().query(uri, null, null, null, null);
		if (cursor.getCount()>0) {
			cursor.moveToFirst();
			result=new City();
			result.setFromCursor(cursor);
		}
		cursor.close();
		return result;
	}
}
