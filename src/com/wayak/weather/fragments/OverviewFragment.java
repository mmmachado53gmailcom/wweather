package com.wayak.weather.fragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.wayak.weather.MainActivity;
import com.wayak.weather.R;
import com.wayak.weather.WeatherService;
import com.wayak.weather.adapters.DaysListAdapter;
import com.wayak.weather.anim.TwoViewFlip;
import com.wayak.weather.helpers.BroadcastHelper;
import com.wayak.weather.libs.ContentHelper;
import com.wayak.weather.models.City;
import com.wayak.weather.models.HourWeatherData;
import com.wayak.weather.models.WeatherData;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public final class OverviewFragment extends Fragment implements OnClickListener {
	private SimpleDateFormat hourDateFormat=new SimpleDateFormat("EEE hh a");
	private SimpleDateFormat timeDateFormat=new SimpleDateFormat("EEE dd h:mm a");
	
	City city;
	private Boolean loading=false;
	TextView temperature_txt;
	TextView description_txt;
	TextView city_txt;
	TextView city2_txt;
	TextView hourTXT;
	TextView forecast_btn;
	Button back_btn;
	ImageView icon_img;
	FragmentManager fManager; 
	private LinearLayout hourLayout;
	private View overView;
	private View forecast;
	private IntentFilter receiverFilter;
	
	private LayoutInflater inflater;
	private List<View> hoursViews=new ArrayList<View>();
	private List<View> hoursViewsCache=new ArrayList<View>();
	
	private DaysListAdapter dayListAdapter;
	private ListView dayList;
	
	private View getHourView(){
		View result;
		if (hoursViewsCache.size()>0) {
			result= hoursViewsCache.remove(hoursViewsCache.size()-1);
		}else{
			result=inflater.inflate(R.layout.view_hour, null);
			HourViewHolder holder=new HourViewHolder();
			holder.hourTXT=(TextView) result.findViewById(R.id.hourTXT);
			holder.iconIMG=(ImageView) result.findViewById(R.id.iconIMG);
			holder.tempTXT=(TextView) result.findViewById(R.id.tempTXT);
			result.setTag(holder);
		}
		hoursViews.add(result);
		return result;
	}
	private void removeAllHoursViews(){
		hourLayout.removeAllViews();
		while (hoursViews.size()>0) {
			hoursViewsCache.add(hoursViews.remove(hoursViews.size()-1));
		}
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		inflater=LayoutInflater.from(getActivity());
		receiverFilter=new IntentFilter();
		receiverFilter.addAction(WeatherService.ACTION_UPDATE_COMPLETE);
		receiverFilter.addAction(WeatherService.ACTION_UPDATE_FAIL);
		receiverFilter.addAction(Intent.ACTION_TIME_TICK);
		getActivity().registerReceiver(receiver, receiverFilter);
		
		fManager=getChildFragmentManager();
		Bundle extras=getArguments();
		int cityId=extras.getInt("cityID");
		
		city=ContentHelper.getCity(cityId, getActivity());
		dayListAdapter=new DaysListAdapter(getActivity(), -1, city);
		TimeZone timezone=TimeZone.getTimeZone(city.timeZoneID);
		hourDateFormat.setTimeZone(timezone);
		timeDateFormat.setTimeZone(timezone);
		loadData();
		Log.i("OverViewFragment","onCreate "+city.name);
	}
	private void loadData(){
		city.overView=ContentHelper.getOverView(city.id, getActivity());
		city.daysWeathers=ContentHelper.getDaysData(city.id, getActivity());
		city.hoursWeathers=ContentHelper.getHoursData(city.id, getActivity());
		dayListAdapter.clear();
		dayListAdapter.addAll(city.daysWeathers);
		if (isVisible()) {
			setDataToView();
		}
		
	}
	
	private void setDataToView(){
		
			icon_img.setImageResource(getIcon(city.overView.icon));
			city_txt.setText(city.name+", "+city.country);
			//hourTXT.setText(city.overView.dateSTB);
			city2_txt.setText(city.name+", "+city.country);
			temperature_txt.setText(city.overView.tempCelsius+"�");
			removeAllHoursViews();
			int n=city.hoursWeathers.size();
			for (int i = 0; i < n; i++) {
				HourWeatherData data=city.hoursWeathers.get(i);
				View hourView=getHourView();
				HourViewHolder h=(HourViewHolder) hourView.getTag();
				h.hourTXT.setText(hourDateFormat.format(data.date));
				h.iconIMG.setImageResource(getIcon(data.icon));
				h.tempTXT.setText(data.tempCelsius+"�");
				hourLayout.addView(hourView);
			}
		
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.i("OverviewFragment","onCreateView");
		View result=inflater.inflate(R.layout.fragment_overview, container,false);
		temperature_txt=(TextView) result.findViewById(R.id.temperature_txt);
		description_txt=(TextView) result.findViewById(R.id.description_txt);
		city_txt=(TextView) result.findViewById(R.id.city_txt);
		city2_txt=(TextView) result.findViewById(R.id.city2_txt);
		hourTXT=(TextView) result.findViewById(R.id.hourTXT1);
		hourTXT.setText(timeDateFormat.format(new Date()));
		icon_img=(ImageView) result.findViewById(R.id.icon_img);
		
		forecast_btn=(TextView) result.findViewById(R.id.forecast_btn);
		forecast_btn.setOnClickListener(this);
		overView=result.findViewById(R.id.overView);
		forecast=result.findViewById(R.id.forecast);
		((Button)result.findViewById(R.id.back_btn)).setOnClickListener(this);
		((Button)result.findViewById(R.id.update_btn)).setOnClickListener(this);
		((Button)result.findViewById(R.id.update2_btn)).setOnClickListener(this);
		hourLayout=(LinearLayout) result.findViewById(R.id.verticalScroll);
		HorizontalScrollView scV=(HorizontalScrollView) result.findViewById(R.id.panel_b);
		dayList=(ListView) result.findViewById(R.id.listView1);
		dayList.setAdapter(dayListAdapter);
		setDataToView();
		return result;
	}
	
	
	
	public static int getIcon(int icon){
		switch(icon){
		case WeatherData.ICON_BROKEN_CLOUDS_DAY:
			return R.drawable.broken_clouds_d;
		case WeatherData.ICON_BROKEN_CLOUDS_NIGHT:
			return R.drawable.broken_clouds_n;
		case WeatherData.ICON_FEW_CLOUDS_DAY:
			return R.drawable.clouds_d;
		case WeatherData.ICON_FEW_CLOUDS_NIGHT:
			return R.drawable.clouds_n;
		case WeatherData.ICON_MISTY:
			return R.drawable.mist;
		case WeatherData.ICON_RAIN:
			return R.drawable.rain;
		case WeatherData.ICON_SCATTERD_CLOUDS:
			return R.drawable.scattered_clouds;
		case WeatherData.ICON_SHOWER_RAIN_DAY:
			return R.drawable.shower_rain_d;
		case WeatherData.ICON_SHOWER_RAIN_NIGHT:
			return R.drawable.shower_rain_n;
		case WeatherData.ICON_SKY_CLEAR_DAY:
			return R.drawable.clear_d;
		case WeatherData.ICON_SKY_CLEAR_NIGHT:
			return R.drawable.clear_n;
		case WeatherData.ICON_SNOW:
			return R.drawable.snow;
		case WeatherData.ICON_THUNDERSTORM:
			return R.drawable.thunderstorm;
			
		}
		 return R.drawable.mist;
	}
	@Override
	public void onClick(View v) {
		int id=v.getId();
		switch (id) {
		case R.id.forecast_btn:
			TwoViewFlip.flipRigth(overView, forecast);
			break;
		case R.id.back_btn:
			TwoViewFlip.flipLeft(forecast, overView);
			break;
		case R.id.update2_btn:
			loading=true;
			BroadcastHelper.sendBroadcast(WeatherService.ACTION_MANUAL_UPDATE, WeatherService.EXTRA_ID, city.id, getActivity());
			break;
		case R.id.update_btn:
			loading=true;
			BroadcastHelper.sendBroadcast(WeatherService.ACTION_MANUAL_UPDATE, WeatherService.EXTRA_ID, city.id, getActivity());
			break;
		case R.id.menu_btn:
			((MainActivity)getActivity()).openMenu();
			break;
		default:
			break;
		}
		
		
	}
	
	private BroadcastReceiver receiver=new BroadcastReceiver(){

		@Override
		public void onReceive(Context context, Intent intent) {
			String action=intent.getAction();
			intent.getIntExtra(WeatherService.EXTRA_ID, -1);
			if (action.equals(WeatherService.ACTION_UPDATE_FAIL)) {
				loading=false;
				if (isVisible()) {
					Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
				}
			}else if(action.equals(WeatherService.ACTION_UPDATE_COMPLETE)){
				int id=intent.getIntExtra(WeatherService.EXTRA_ID, -1);
				if (city.id==id) {
					loading=false;
					loadData();
				}
			}else if(action.equals(Intent.ACTION_TIME_TICK)){
				hourTXT.setText(timeDateFormat.format(new Date()));
			}
			
		}
		
	};
	
	public void onDestroy() {
		getActivity().unregisterReceiver(receiver);
		super.onDestroy();
	};
	
	private class HourViewHolder{
		TextView hourTXT;
		TextView tempTXT;
		ImageView iconIMG;
	}
	@Override
	public void onDestroyView() {
		removeAllHoursViews();
		super.onDestroyView();
	}
}
