package com.wayak.weather.db;

public interface DataSource {
	void open();
	void close();
}
