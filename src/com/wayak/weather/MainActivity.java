package com.wayak.weather;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import android.provider.ContactsContract;

import com.wayak.weather.anim.ZoomOutPageTransformer;
import com.wayak.weather.db.CitiesDataSource;

import com.wayak.weather.fragments.OverviewFragment;
import com.wayak.weather.models.City;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;

public class MainActivity extends FragmentActivity {
	private Adapter adapter;
	private ViewPager pager;
	private Intent service;
	private Context ctx;
	private DrawerLayout drawer;
	private LinearLayout leftMenu;
	private List<City> cities;
	private CitiesDataSource citiesData;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ctx=getApplicationContext();
		citiesData=CitiesDataSource.get(ctx);
		citiesData.open();
		cities=citiesData.getAllCities(false);
		citiesData.close();
		Log.i("machado_test","cities: "+cities.size());
		adapter=new Adapter(getSupportFragmentManager());
		adapter.addAll(cities);
		setContentView(R.layout.activity_main);
		leftMenu= (LinearLayout) findViewById(R.id.left_menu);
		pager=(ViewPager) findViewById(R.id.pager);
		drawer=(DrawerLayout) findViewById(R.id.drawer);
		pager.setPageTransformer(true, new ZoomOutPageTransformer());
		pager.setAdapter(adapter);
		
		service=new Intent(ctx, WeatherService.class);
		startService(service);
		//getOverView();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	private class Adapter extends FragmentPagerAdapter{
		private List<Fragment> fragments=new ArrayList<Fragment>();
		public Adapter(FragmentManager fm) {
			super(fm);
		}
		
		public void add(City city){
			OverviewFragment fragment=new OverviewFragment();
			Bundle extras=new Bundle();
			extras.putInt("cityID", city.id);
			fragment.setArguments(extras);
			fragments.add(fragment);
			notifyDataSetChanged();
		}
		public void addAll(List<City> cities){
			fragments.clear();
			for (City city : cities) {
				OverviewFragment fragment=new OverviewFragment();
				Bundle extras=new Bundle();
				extras.putInt("cityID", city.id);
				fragment.setArguments(extras);
				fragments.add(fragment);
			}
			notifyDataSetChanged();
		}
		
		public void clear(){
			fragments.clear();
			notifyDataSetChanged();
		}
		@Override
		public Fragment getItem(int pos) {
			return fragments.get(pos);
		}

		@Override
		public int getCount() {
			return fragments.size();
		}
		
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		stopService(service);
		super.onDestroy();
	}
	
	public void launchSearch(View v){
		Intent i=new Intent(MainActivity.this, SearchActivity.class);
		startActivityForResult(i, 0);
		//startActivity(i);
	}
	
	public void getOverView(){
		Uri uri=Uri.parse("content://" + WeatherContentProvider.AUTHORITY +"/"+WeatherContentProvider.PATH_OVER_VIEW_FORECAST+"/4012176");
		Cursor cursor=getContentResolver().query(uri, null, null, null, null);
		Log.i("MainActivity", "n: "+cursor.getCount());
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultcode, Intent intent) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultcode, intent);
		if (resultcode == RESULT_OK) {
			int cityId=intent.getIntExtra("city", -1);
			Log.i("MainActivity","cityID= "+cityId);
		}
		
	}
	
	public void openMenu(){
		drawer.openDrawer(leftMenu);
	}
}
