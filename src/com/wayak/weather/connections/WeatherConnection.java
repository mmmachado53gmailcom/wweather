package com.wayak.weather.connections;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONException;

import com.wayak.weather.models.City;
import com.wayak.weather.models.DayWeatherData;
import com.wayak.weather.models.HourWeatherData;



public abstract class WeatherConnection {
	int currentError=0;
	abstract String getSearchCityURL(Double lat, Double lon);
	abstract String getSearchCityURL(String CityName);
	//abstract List<City> searchCity(Double lat, Double lon) throws ClientProtocolException, JSONException, IOException;
	abstract List<City> searchCity(HttpGet get) throws ClientProtocolException, JSONException, IOException;
	
	abstract String getForecastHourURL(int cityId);
	abstract String getForecastHourURL(String cityName);
	abstract String getForecastHourURL(Double lat, Double lon);
	abstract List<HourWeatherData> getForecastHour(HttpGet get) throws ClientProtocolException, JSONException, IOException;
	/*abstract List<HourWeatherData> getForecastHour(String cityName) throws ClientProtocolException, JSONException, IOException;
	abstract List<HourWeatherData> getForecastHour(Double lat, Double lon) throws ClientProtocolException, JSONException, IOException;*/
	abstract String getForecastDayURL(int cityId);
	abstract String getForecastDayURL(String cityName);
	abstract String getForecastDayURL(Double lat, Double lon);
	abstract List<DayWeatherData> getForecastDay(HttpGet get) throws ClientProtocolException, JSONException, IOException;
	/*abstract List<DayWeatherData> getForecastDay(String cityName) throws ClientProtocolException, JSONException, IOException;
	abstract List<DayWeatherData> getForecastDay(Double lat, Double lon) throws ClientProtocolException, JSONException, IOException;*/
	abstract String getOverViewURL(String cityName);
	abstract String getOverViewURL(int cityID);
	abstract String getOverViewURL(Double lat, Double lon);
	abstract DayWeatherData getOverView(HttpGet get) throws ClientProtocolException, JSONException, IOException;
	/*abstract DayWeatherData getOverView(int cityID) throws ClientProtocolException, JSONException, IOException;
	abstract DayWeatherData getOverView(Double lat, Double lon) throws ClientProtocolException, JSONException, IOException;*/
	
	public static int ERROR_CONNECTION=1;
	public static int ERROR_SERVER=2;
	public static int ERROR_UKNOW=3;
}
