package com.wayak.weather.helpers;

import android.content.Context;
import android.content.Intent;

public class BroadcastHelper {
	public static String EXTRA_MSG="msg";
	
	public static void sendBroadcast(String action, Context ctx){
		Intent intent=new Intent();
		intent.setAction(action);
		ctx.sendBroadcast(intent);
	}
	
	public static void sendBroadcast(String action, String msg, Context ctx){
		Intent intent=new Intent();
		intent.setAction(action);
		intent.putExtra(EXTRA_MSG, msg);
		ctx.sendBroadcast(intent);
	}
	
	public static void sendBroadcast(String action, String extraName, int extraValue, Context ctx){
		Intent intent=new Intent();
		intent.setAction(action);
		intent.putExtra(extraName, extraValue);
		ctx.sendBroadcast(intent);
	}
	
	public static void sendBroadcast(String action, String extraName, String extraValue, Context ctx){
		Intent intent=new Intent();
		intent.setAction(action);
		intent.putExtra(extraName, extraValue);
		ctx.sendBroadcast(intent);
	}
}
