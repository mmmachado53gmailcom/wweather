package com.wayak.weather;

import java.util.Date;

import com.wayak.weather.db.AppSQLiteHelper;
import com.wayak.weather.db.Schema;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public final class WeatherContentProvider extends ContentProvider {
	private AppSQLiteHelper sqliteHelper;
	
	public static final String AUTHORITY = "com.wayak.weather.weathercontentprovider";
	public static final String PATH_OVER_VIEW_FORECAST="overView";
	public static final String PATH_HOUR_FORECAST="hour";
	public static final String PATH_DAY_FORECAST="day";
	public static final String PATH_CITY="city";

	
	private static final int OVER_VIEW=0;
	private static final int HOUR=1;
	private static final int DAY=2;
	private static final int CITY=3;
	private static final UriMatcher matcher=new UriMatcher(UriMatcher.NO_MATCH);
	static{
		matcher.addURI(AUTHORITY, PATH_OVER_VIEW_FORECAST+"/#", OVER_VIEW);
		matcher.addURI(AUTHORITY, PATH_HOUR_FORECAST+"/#", HOUR);
		matcher.addURI(AUTHORITY, PATH_DAY_FORECAST+"/#", DAY);
		matcher.addURI(AUTHORITY, PATH_CITY+"/#", CITY);
	}
	@Override
	public int delete(Uri arg0, String arg1, String[] arg2) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getType(Uri arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri arg0, ContentValues arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean onCreate() {
		sqliteHelper=new AppSQLiteHelper(getContext());
		return false;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,String[] selectionArgs, String sortOrder) {
		SQLiteDatabase db=sqliteHelper.getReadableDatabase();
		SQLiteQueryBuilder queryBuilder=new SQLiteQueryBuilder();
		long now=new Date().getTime()/1000L;
		
		String cityId=uri.getPathSegments().get(1);
		
		switch (matcher.match(uri)) {
		case OVER_VIEW:
			queryBuilder.setTables(Schema.WeatherDatas.TABLE_NAME);
			queryBuilder.appendWhere(Schema.WeatherDatas.KEY_CITY_ID+" = "+cityId+" AND ");
			queryBuilder.appendWhere(Schema.WeatherDatas.KEY_TYPE+" = "+Schema.WeatherDatas.TYPE_OVERVIEW);
			break;
		case DAY:
			queryBuilder.setTables(Schema.WeatherDatas.TABLE_NAME);
			queryBuilder.appendWhere(Schema.WeatherDatas.KEY_CITY_ID+" = "+cityId+" AND ");
			queryBuilder.appendWhere(Schema.WeatherDatas.KEY_TYPE+" = "+Schema.WeatherDatas.TYPE_DAY+" AND ");
			queryBuilder.appendWhere(Schema.WeatherDatas.KEY_DT+" > "+now);
			break;
		case HOUR:
			queryBuilder.setTables(Schema.WeatherDatas.TABLE_NAME);
			queryBuilder.appendWhere(Schema.WeatherDatas.KEY_CITY_ID+" = "+cityId+" AND ");
			queryBuilder.appendWhere(Schema.WeatherDatas.KEY_TYPE+" = "+Schema.WeatherDatas.TYPE_HOUR+" AND ");
			queryBuilder.appendWhere(Schema.WeatherDatas.KEY_DT+" > "+now);
			break;
		case CITY:
			queryBuilder.setTables(Schema.Cityes.TABLE_NAME);
			queryBuilder.appendWhere(Schema.Cityes.KEY_ID+" = "+cityId);
			break;
		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
		return queryBuilder.query(db, null, null, null, null, null, null);
	}

	@Override
	public int update(Uri arg0, ContentValues arg1, String arg2, String[] arg3) {
		// TODO Auto-generated method stub
		return 0;
	}

}
