package com.wayak.weather;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONException;

import com.wayak.weather.config.Config;
import com.wayak.weather.connections.OpenWeatherMapConnection;
import com.wayak.weather.db.CitiesDataSource;
import com.wayak.weather.helpers.BroadcastHelper;
import com.wayak.weather.models.City;
import com.wayak.weather.models.DayWeatherData;
import com.wayak.weather.models.HourWeatherData;


import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public final class WeatherService extends Service {
	public static final String ACTION_UPDATE_REQUEST="wweather_update_request";
	public static final String ACTION_MANUAL_UPDATE="wweather_manual_update";
	public static final String ACTION_UPDATE_COMPLETE="wweather_update_complete";
	public static final String ACTION_UPDATE_FAIL="wweather_update_fail";
	
	public static final String EXTRA_ID="id";
	

	
	private static PendingIntent servicePendingIntent;
	private static Intent serviceIntent;
	private static Context ctx;
	private static final long FORCE_DOWNLOAD_TIME=300000;
	private static UpdateTask updateTask=null;
	//private customBroadcastReceiver receiv=new customBroadcastReceiver();
	
	
	private static AlarmManager alarmManager=null;
	
	private static IntentFilter receiverFilter=new IntentFilter();
	
	private CitiesDataSource citiesDataSource;
	private OpenWeatherMapConnection connection;
	
	static{
		receiverFilter.addAction(ACTION_UPDATE_REQUEST);
		receiverFilter.addAction(ACTION_MANUAL_UPDATE);
		//receiverFilter.addAction(Intent.ACTION_SCREEN_ON);
	}
	
	private static Intent INTENT_ACTION_UPDATE_COPLETE=new Intent(ACTION_UPDATE_COMPLETE);
	private static Intent INTENT_ACTION_UPDATE_FAILD=new Intent(ACTION_UPDATE_FAIL);
	
	@Override
	public IBinder onBind(Intent data) {
		// TODO Auto-generated method stub
		return null;
		
	}
	@Override
	public void onCreate() {
		Log.i("WeatherService", "oncreate");
		ctx=getApplicationContext();
		connection=(OpenWeatherMapConnection) OpenWeatherMapConnection.get(ctx);
		citiesDataSource=CitiesDataSource.get(ctx);
		serviceIntent=new Intent(ctx, WeatherService.class);
		serviceIntent.setAction(ACTION_UPDATE_REQUEST);
		servicePendingIntent=PendingIntent.getService(ctx, 0, serviceIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		alarmManager=(AlarmManager) getSystemService(ALARM_SERVICE);
		alarmManager.cancel(servicePendingIntent);
		//init();
		//update();
		//Config config=Config.get(ctx);
		//long defaultUpdateTime=config.getUpdateTime()*3600000;
		//long defaultUpdateTime=config.getUpdateTime()*1000;
		
		//long timeElapsed=getTimeElapsed();
		//if (timeElapsed>=defaultUpdateTime) {
			//update();
		//}else{
		//	long newTime=defaultUpdateTime-timeElapsed;
		//}
		
		
		// TODO Auto-generated method stub
		registerReceiver(receiver, receiverFilter);
		super.onCreate();
	}
	private BroadcastReceiver receiver=new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			
			String action=intent.getAction();
			Log.i("WeatherService", "onReceive: "+action);
			update(action);
		}
	};
	public void onDestroy() {
		unregisterReceiver(receiver);
		alarmManager.cancel(servicePendingIntent);
		Log.i("WeatherService", "onDesctroy");
		super.onDestroy();
	};
	

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		Log.i("WeatherService", "onStartCommand");
		if (intent.getAction()!=null) {
			String action=intent.getAction();
			Log.i("WeatherService", "onStartCommand action: "+action);
			if (action.equals(ACTION_MANUAL_UPDATE) ) {
				update(ACTION_MANUAL_UPDATE);
				return super.onStartCommand(intent, flags, startId);
			}
			
		}
		update(ACTION_UPDATE_REQUEST);
		//String action=intent.getAction();
		/*if (action.equals(ACTION_UPDATE_REQUEST)) {
			
		}*/
		
		//Log.i("WeatherService", "onStartCommand: "+action);
		return super.onStartCommand(intent, flags, startId);
	}
	
	/*private long getTimeElapsed(){
		Config config=Config.get(ctx);
		long lastUpdate=config.getLastUpdateTime();
		long timeNow=new Date().getTime();
		return timeNow-lastUpdate;
	}*/
	
	private void update(String action){
		alarmManager.cancel(servicePendingIntent);
		if (updateTask==null) {
			updateTask=new UpdateTask(this);
			updateTask.execute(action);
		}
	}
	
	protected void onUpdateError(){
		BroadcastHelper.sendBroadcast(ACTION_UPDATE_FAIL, ctx);
		updateTask=null;
		alarmManager.cancel(servicePendingIntent);
		alarmManager.setRepeating(AlarmManager.RTC, new Date().getTime()+FORCE_DOWNLOAD_TIME, FORCE_DOWNLOAD_TIME, servicePendingIntent);
	}
	
	private void onUpdateComlete(){
		BroadcastHelper.sendBroadcast(ACTION_UPDATE_COMPLETE, ctx);
		updateTask=null;
		alarmManager.cancel(servicePendingIntent);
		citiesDataSource.open();
		List<City> cities=citiesDataSource.getOutdatedCities();
		citiesDataSource.close();
		if (cities.size()>0) {
			update(ACTION_UPDATE_REQUEST);
		return;
		}
		long defaultUpdateTime=Config.get(ctx).getUpdateTime()*60000;
		alarmManager.setRepeating(AlarmManager.RTC, new Date().getTime()+defaultUpdateTime, defaultUpdateTime, servicePendingIntent);
	}
	
	private class UpdateTask extends AsyncTask<String, Integer, Boolean>{
		private WeatherService parent;
		private HttpGet get=null;
		public UpdateTask(WeatherService p){
			parent=p;
		}
		@Override
		protected void onProgressUpdate(Integer... values) {
			/*new AlertDialog.Builder(getApplicationContext())
			.setTitle("Service")
			.setMessage(values[0]).setPositiveButton("ok",null).create().show();*/
			Toast.makeText(getApplicationContext(), "city: "+values[0], Toast.LENGTH_LONG).show();
			BroadcastHelper.sendBroadcast(ACTION_UPDATE_COMPLETE, EXTRA_ID, values[0], parent);
			super.onProgressUpdate(values);
			
		}
		@Override
		protected Boolean doInBackground(String... params) {
			String action=params[0];
			List<City> cities;
			parent.citiesDataSource.open();
			if (action.equals(ACTION_UPDATE_REQUEST)) {
				cities=parent.citiesDataSource.getOutdatedCities();
			}else if(action.equals(ACTION_MANUAL_UPDATE)){
				cities=parent.citiesDataSource.getAllCities(false);
			}else{
				parent.citiesDataSource.close();
				return true;
			}
			 
			
			if (cities.size()==0) {
				parent.citiesDataSource.close();
				return true;
			}
			
			for(City city : cities){
				
				try {
					//publishProgress("Satart update "+city.name);
					
					Log.i("WeatherService","update: "+city.name);
					String url=connection.getOverViewURL(city.id);
					get=new HttpGet(url);
					DayWeatherData overView=connection.getOverView(get);
					url=connection.getForecastHourURL(city.id);
					get=new HttpGet(url);
					List<HourWeatherData> hourWeathers=connection.getForecastHour(get);
					url=connection.getForecastDayURL(city.id);
					get=new HttpGet(url);
					List<DayWeatherData> dayWeathers=connection.getForecastDay(get);
					parent.citiesDataSource.saveForecast(dayWeathers, hourWeathers, overView, city.id);
					//publishProgress(city.name+" temp:"+overView.temp);
					Log.i("WeatherService",city.name+" temp:"+overView.temp);
				} catch (ClientProtocolException e) {
					//publishProgress("error update "+city.name);
					cancel(true);
					e.printStackTrace();
					return true;
				} catch (JSONException e) {
					//publishProgress("error update "+city.name);
					
					e.printStackTrace();
				} catch (IOException e) {
					//publishProgress("error update "+city.name);
					cancel(true);
					e.printStackTrace();
					return true;
				}
				city.last_update=new Date().getTime();
				parent.citiesDataSource.saveCity(city);
				publishProgress(city.id);
			}
			parent.citiesDataSource.close();
			return true;
		}
		
		@Override
		protected void onCancelled() {
			parent.onUpdateError();
			super.onCancelled();
		}
	
		@Override
		protected void onPostExecute(Boolean result) {
			parent.onUpdateComlete();
			super.onPostExecute(result);
		}
		
		public void stop(){
			if (get!=null) {
				get.abort();
			}
			cancel(true);
		}
		
	}
	
	
	

}
