package com.wayak.weather.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class AppSQLiteHelper extends SQLiteOpenHelper {
	private static final String DB_FILE="weather.db";
	private static final int DB_VERSION=2;

	public AppSQLiteHelper(Context context) {
		super(context, DB_FILE, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(Schema.Cityes.QUERY_CREATE);
		db.execSQL(Schema.WeatherDatas.QUERY_CREATE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL(Schema.Cityes.QUERY_UPDATE);
		db.execSQL(Schema.WeatherDatas.QUERY_UPDATE);

	}

}
