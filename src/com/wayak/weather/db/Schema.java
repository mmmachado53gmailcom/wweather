package com.wayak.weather.db;

public final class Schema {
	public static class Cityes{
		public static final String TABLE_NAME="cities";
		public static final String KEY_ID="id";
		public static final String KEY_NAME="name";
		public static final String KEY_LAT="lat";
		public static final String KEY_LON="lon";
		public static final String KEY_COUNTRY="country";
		public static final String KEY_LAST_UPDATE="last_update";
		public static final String KEY_TIMEZONE_ID="timezone_id";

		public static final String QUERY_CREATE="CREATE TABLE if not exists "+TABLE_NAME
				+" ( "
				+KEY_ID+" integer PRIMARY KEY,"
				+KEY_NAME+" text DEFAULT '',"
				+KEY_TIMEZONE_ID+","
				+KEY_LAT+" real DEFAULT 0.0,"
				+KEY_LON+" real DEFAULT 0.0,"
				+KEY_LAST_UPDATE+" real DEFAULT 0,"
				+KEY_COUNTRY+" text DEFAULT ''"
				+" )";
		public static final String QUERY_UPDATE="DROP TABLE IF EXISTS "+TABLE_NAME;
		
	}
	public static class WeatherDatas{
		public static final String TABLE_NAME="weather_datas";
		public static final String KEY_ID="id";
		public static final String KEY_CITY_ID="city_id";
		public static final String KEY_TYPE="type";
		public static final String KEY_ICON="icon";
		public static final String KEY_DT="dt";
		public static final String KEY_TEMP="temp";
		public static final String KEY_PRESSURE="pressure";
		public static final String KEY_HUMIDITY="humidity";
		public static final String KEY_TEMP_MIN="temp_min";
		public static final String KEY_TEMP_MAX="temp_max";
		public static final String KEY_WIND_SPEED="wind_speed";
		public static final String KEY_WIND_DEG="wind_deg";
		public static final String KEY_SUNSET="sunset";
		public static final String KEY_SUNRISE="sunrise";
		public static final String KEY_DESCRIPTION_CODE="description_code";
		


		public static final String QUERY_CREATE="CREATE TABLE if not exists "+TABLE_NAME
				+" ( "
				+KEY_ID+" integer PRIMARY KEY,"
				+KEY_CITY_ID+" integer not null,"
				+KEY_TYPE+" integer not null,"
				+KEY_ICON+" integer not null,"
				+KEY_DT+" integer not null,"	
				+KEY_TEMP+" real DEFAULT 0.0,"				
				+KEY_PRESSURE+" real DEFAULT 0.0,"				
				+KEY_HUMIDITY+" integer DEFAULT 0,"
				+KEY_TEMP_MIN+" real DEFAULT 0.0,"				
				+KEY_TEMP_MAX+" real DEFAULT 0.0,"	
				+KEY_SUNSET+" integer DEFAULT 0,"
				+KEY_SUNRISE+" integer DEFAULT 0,"
				+KEY_WIND_SPEED+" real DEFAULT 0.0,"				
				+KEY_WIND_DEG+" real DEFAULT 0.0,"			
				+KEY_DESCRIPTION_CODE+" integer DEFAULT 0 "	
				+" )";
		public static final int TYPE_OVERVIEW=0;
		public static final int TYPE_DAY=1;
		public static final int TYPE_HOUR=2;
		public static final String QUERY_UPDATE="DROP TABLE IF EXISTS "+TABLE_NAME;
		
	}
}
