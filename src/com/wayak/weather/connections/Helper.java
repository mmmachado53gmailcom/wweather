package com.wayak.weather.connections;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public final class Helper {
	
	// need AsyncTask
	public static JSONArray getJsonArray(Context ctx, HttpGet get) throws ClientProtocolException, IOException, JSONException{
		String request=getRequest(ctx, get);
		if (request!=null) {
			JSONArray array=new JSONArray(request);
			return array;			
		}
		return null;
	}
	
	// need AsyncTask
	public static JSONObject getJsonObject(Context ctx, HttpGet get) throws JSONException, ClientProtocolException, IOException{
		String request=getRequest(ctx, get);
		return new JSONObject(request);			
		
	}
	
	// need AsyncTask
	public static String getRequest(Context ctx, HttpGet get) throws ClientProtocolException, IOException{
		StringBuilder builder = new StringBuilder();
    	HttpClient cliente=new DefaultHttpClient();
    	//HttpGet get=new HttpGet(url);
			HttpResponse response=cliente.execute(get);
			StatusLine statusline=response.getStatusLine();
			int statuscode=statusline.getStatusCode();
			//if(statuscode==200){
				InputStream contenido=response.getEntity().getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(contenido));
				String line;
	            while ((line = reader.readLine()) != null) {
	              builder.append(line);
	            }
	            return builder.toString();
			/*}else {
	            Log.e("error", "Failed to download file");
	          }*/
		
    	//return null;
	}
	
	public class ConnectionError extends Exception{
		public ConnectionError(String message){
			super(message);
		}
	}
}
