package com.wayak.weather.models;

import java.io.Serializable;

import com.wayak.weather.db.Schema;

import android.content.ContentValues;

public final class HourWeatherData extends WeatherData implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int hour;

	@Override
	public ContentValues toContentValues() {
		ContentValues result=super.toContentValues();
		result.put(Schema.WeatherDatas.KEY_TYPE, Schema.WeatherDatas.TYPE_HOUR);
		return result;
	}
	
}
