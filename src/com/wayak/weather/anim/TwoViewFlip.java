package com.wayak.weather.anim;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.view.View;

public class TwoViewFlip {
	
	private TwoViewFlip(){}
	private static long DURATION=500;
	
	public static void flipRigth(final View faceA, final View faceB){
		
		flip(true, faceA,faceB);
	}
	
public static void flipLeft(final View faceA, final View faceB){
		
		flip(false, faceA, faceB);
	}
	
	
		
	private static void flip(final Boolean right, final View faceA, final View faceB){
		
		faceB.setVisibility(View.GONE);
		faceA.setVisibility(View.VISIBLE);
		faceA.setRotationY(0);
		faceA.animate().rotationY(right == true ? 90 : -90).setDuration(DURATION/2).alpha(0.5f).scaleX(0.65f).scaleY(0.65f).setListener(new AnimatorListener() {
			
			@Override
			public void onAnimationStart(Animator animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationRepeat(Animator animation) {
							
			}
			
			@Override
			public void onAnimationEnd(Animator animation) {
				faceA.animate().setListener(null);	
				faceA.setVisibility(View.GONE);
				/*faceA.setRotationY(0);
				faceA.setAlpha(1);
				faceA.setScaleX(1);
				faceA.setScaleY(1);*/
				faceB.setRotationY(right == true ? -90 : 90);
				faceB.setAlpha(0.85f);
				faceB.setVisibility(View.VISIBLE);
				faceB.animate().rotationY(0).setDuration(DURATION/2).alpha(1).scaleX(1).scaleY(1).start();
			}
			
			@Override
			public void onAnimationCancel(Animator animation) {
				// TODO Auto-generated method stub
				
			}
		}).start();
	}
}
