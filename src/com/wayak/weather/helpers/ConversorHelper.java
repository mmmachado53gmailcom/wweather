package com.wayak.weather.helpers;

public class ConversorHelper {
	public static int fToc(int f){
		return ((f-32)*5)/9;
	}
	public static int kelvinToCelsius(int k){
		return k-273;
	}
	public static int kelvinToFahrenheit(int k){
		return (int) (7.8*(k-273)+32);
	}
}
