package com.wayak.weather.theme;

import com.wayak.weather.R;
import com.wayak.weather.models.WeatherData;

public class ThemeHelper {
	public static int getIcon(int icon){
		switch(icon){
		case WeatherData.ICON_BROKEN_CLOUDS_DAY:
			return R.drawable.broken_clouds_d;
		case WeatherData.ICON_BROKEN_CLOUDS_NIGHT:
			return R.drawable.broken_clouds_n;
		case WeatherData.ICON_FEW_CLOUDS_DAY:
			return R.drawable.clouds_d;
		case WeatherData.ICON_FEW_CLOUDS_NIGHT:
			return R.drawable.clouds_n;
		case WeatherData.ICON_MISTY:
			return R.drawable.mist;
		case WeatherData.ICON_RAIN:
			return R.drawable.rain;
		case WeatherData.ICON_SCATTERD_CLOUDS:
			return R.drawable.scattered_clouds;
		case WeatherData.ICON_SHOWER_RAIN_DAY:
			return R.drawable.shower_rain_d;
		case WeatherData.ICON_SHOWER_RAIN_NIGHT:
			return R.drawable.shower_rain_n;
		case WeatherData.ICON_SKY_CLEAR_DAY:
			return R.drawable.clear_d;
		case WeatherData.ICON_SKY_CLEAR_NIGHT:
			return R.drawable.clear_n;
		case WeatherData.ICON_SNOW:
			return R.drawable.snow;
		case WeatherData.ICON_THUNDERSTORM:
			return R.drawable.thunderstorm;
			
		}
		 return R.drawable.mist;
	}
}
