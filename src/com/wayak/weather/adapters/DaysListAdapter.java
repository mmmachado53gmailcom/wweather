package com.wayak.weather.adapters;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

import com.wayak.weather.R;
import com.wayak.weather.fragments.OverviewFragment;
import com.wayak.weather.models.City;
import com.wayak.weather.models.DayWeatherData;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class DaysListAdapter extends ArrayAdapter<DayWeatherData> {
	private SimpleDateFormat dayDateFormat=new SimpleDateFormat("EEE");
	private SimpleDateFormat dateDateFormat=new SimpleDateFormat("MMM d");
	LayoutInflater inflater;
	
	public DaysListAdapter(Context context, int textViewResourceId, City city) {
		super(context, textViewResourceId);
		inflater=LayoutInflater.from(context);
		TimeZone tz=TimeZone.getTimeZone(city.timeZoneID);
		dayDateFormat.setTimeZone(tz);
		dateDateFormat.setTimeZone(tz);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		DayWeatherData data=getItem(position);
		Holder holder;
		if (convertView==null) {
			convertView=inflater.inflate(R.layout.view_day, null);
			holder=new Holder();
			holder.dateTXT=(TextView) convertView.findViewById(R.id.dateTXT);
			holder.dayTXT=(TextView) convertView.findViewById(R.id.dayTXT);
			holder.iconIMG=(ImageView) convertView.findViewById(R.id.iconIMG);
			holder.tempTXT=(TextView) convertView.findViewById(R.id.tempTXT);
			convertView.setTag(holder);
		}else{
			holder=(Holder) convertView.getTag();
		}
		holder.dayTXT.setText(dayDateFormat.format(data.date));
		holder.dateTXT.setText(dateDateFormat.format(data.date));
		holder.tempTXT.setText(data.temp_min_celsius+"�/"+data.temp_max_celsius+"�");
		holder.iconIMG.setImageResource(OverviewFragment.getIcon(data.icon));
		return convertView;
	}
	
	private class Holder{
		TextView dayTXT;
		TextView dateTXT;
		TextView tempTXT;
		ImageView iconIMG;
	}
	

}
