package com.wayak.weather.config;

import android.content.Context;
import android.content.SharedPreferences;

public final class Config {
	private static Config SINGLETON=null;
	private static Context ctx=null;
	private static SharedPreferences WRITEABLE_PREFERENCE=null;
	private static SharedPreferences READABLE_PREFERENCE=null;
	private static final String PREFERENCES_NAME="preferences";
	
	
	private static final String UPDATE_TIME="update_time";
	private static final String LAST_UPDATE_TIME="last_update_time";
	
	public static Config get(Context context){
		ctx=context;
		if (SINGLETON==null) {
			WRITEABLE_PREFERENCE=ctx.getSharedPreferences(PREFERENCES_NAME, Context.MODE_WORLD_WRITEABLE);
			READABLE_PREFERENCE=ctx.getSharedPreferences(PREFERENCES_NAME, Context.MODE_WORLD_READABLE);
			SINGLETON=new Config();
		}
		return SINGLETON;
	}
	
	public int getUpdateTime(){
		return READABLE_PREFERENCE.getInt(UPDATE_TIME, 60);
	}
	public long getLastUpdateTime(){
		return READABLE_PREFERENCE.getLong(LAST_UPDATE_TIME, 0);
	}
	public void setUpdateTime(int time){
		SharedPreferences.Editor editor=READABLE_PREFERENCE.edit();
		editor.putInt(UPDATE_TIME, time);
		editor.commit();
	}
	public void setLastUpdateTime(int time){
		SharedPreferences.Editor editor=READABLE_PREFERENCE.edit();
		editor.putLong(LAST_UPDATE_TIME, time);
	}
	
}
